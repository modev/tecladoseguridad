import Keyboard from 'simple-keyboard';
import 'simple-keyboard/build/css/index.css';

import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}

}
